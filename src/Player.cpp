#include "Player.hpp"
#include <string>
#include <iostream>

Player::Player(){
    nome = "jogador";
    pontos = 0;
}

Player::~Player(){
    
}

std::string Player::get_mapa(){
    return nome + ".txt";
}

int Player::get_pontos(){
    return pontos;
}

void Player::set_nome(std::string nome){
    this->nome = nome;
}

std::string Player::get_nome(){
    return nome;
}

void Player::acertou_tiro(){
    pontos++;
}

bool Player::recebe_tiro(int linha, int coluna){
    bool embarcacao_viva;
    if ((navios[{linha, coluna}]!=NULL&&navios[{linha, coluna}]->is_crash(linha, coluna))||tiro_agua[{linha, coluna}]){
        std::cout<<"Voce ja atirou nessa posicao"<<std::endl;
        return false;
    }
    else if (navios[{linha, coluna}]==NULL){
        TirosNaAgua(linha, coluna);
        std::cout<<"Que pena!! Tiro na agua"<<std::endl;
        return false;
    }
    else {
        embarcacao_viva = navios[{linha, coluna}]->recebe_dano(linha, coluna);
    }
    return !embarcacao_viva;
}

void Player::Le_Mapa(int qtde_canoa, int qtde_porta_avioes, int qtde_submarinos){
    int i, linha, coluna;
    for (i=1; i<=qtde_canoa; i++){
        std::cout<<nome<<" insira a linha da sua canoa "<<i<<":\n";
        std::cin>>linha;
        std::cout<<nome<<" insira a coluna da sua canoa "<<i<<":\n";
        std::cin>>coluna;

        set_canoa(linha, coluna);
        system("clear");
    }

    for (i=1; i<=qtde_submarinos; i++){
        std::string orientacao;
        std::cout<<nome<<" insira a linha do seu submarino "<<i<<":\n";
        std::cin>>linha;
        std::cout<<nome<<" insira a coluna do seu submarino "<<i<<":\n";
        std::cin>>coluna;
        std::cout<<"Insira a orientação: (cima, baixo, esquerda, direita)\n";
        std::cin>>orientacao;
        set_submarino(linha, coluna, orientacao);

        system("clear");
    }

    for (i=1; i<=qtde_porta_avioes; i++){
        std::string orientacao;
        std::cout<<nome<<" insira a linha do seu porta aviões "<<i<<":\n";
        std::cin>>linha;
        std::cout<<nome<<" insira a coluna do seu porta aviões "<<i<<":\n";
        std::cin>>coluna;
        std::cout<<"Insira a orientação: (cima, baixo, esquerda, direita)\n";
        std::cin>>orientacao;
        set_porta_avioes(linha, coluna, orientacao);

        system ("clear");
    }
}
