#include "Partida.hpp"
#include "Player.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

Partida::Partida(){
    std::cout<<"Vamos comecar a partida\n\n\n"<<std::endl;
}

Partida::~Partida(){

}
void Partida::preparar_partida(){
    
    Configura_Player();
    Tamanho_mapa();
    
    player1.carrega_mapa(player1.get_mapa());
    player2.carrega_mapa(player2.get_mapa());
}

void Partida::Game(){

    srand((unsigned)time(0)); // gerar numero aleatório para sortear quem comeca
    if (rand() % 2==1){
        std::swap(player1, player2);
    }
    std::cout<<"\t\t"<<player1.get_nome()<<" comeca jogando!!!"<<std::endl;
    usleep(1000000);
    system("clear");

    while (true){
        Vez_Jogador(&player1, &player2);
        if (checa_ganhador(&player1)){
            Declara_Vencedor(&player1);
            usleep(1000000);
            break;
        }
        Vez_Jogador(&player2, &player1);
        if (checa_ganhador(&player2)){
            Declara_Vencedor(&player2);
            usleep(1000000);
            break;
        }
    }
}

void Partida::Vez_Jogador(Player *player1, Player *player2){
    int linha, coluna;
    if (player1->get_nome()<player2->get_nome()){
        std::cout<<player1->get_nome()<<"\t"<<player1->get_pontos()<<std::endl;
        std::cout<<player2->get_nome()<<"\t"<<player2->get_pontos()<<std::endl;
    }
    else {
        std::cout<<player2->get_nome()<<"\t"<<player2->get_pontos()<<std::endl;
        std::cout<<player1->get_nome()<<"\t"<<player1->get_pontos()<<std::endl;
    }
        
        player2->imprime_mapa();
        
        std::cout<<player1->get_nome()<<" insira a linha do seu ataque ";
        std::cin>>linha;
        std::cout<<std::endl<<player1->get_nome()<<" insira a coluna do seu ataque ";
        std::cin>>coluna;

        if (player2->recebe_tiro(linha, coluna)){
            player1->acertou_tiro();
        }
        usleep(2000000);
        system("clear");
}

void Partida::Inserir_Mapa(){
    int qtde_canoa, qtde_submarino, qtde_porta_avioes;

    Tamanho_mapa();

    std::cout<<"Insira a quantidade de canoas do jogo:\n";
    std::cin>>qtde_canoa;
    std::cout<<"\nInsira a quantidade de subamrinos\n";
    std::cin>>qtde_submarino;
    std::cout<<"\nInsira a quantidade de Porta Aviões\n";
    std::cin>>qtde_porta_avioes;

    Configura_Player();
    system("clear");
    player1.Le_Mapa(qtde_canoa, qtde_porta_avioes, qtde_submarino);
    player2.Le_Mapa(qtde_canoa, qtde_porta_avioes, qtde_submarino);
}

void Partida::Configura_Player(){
    std::string nome_player1, nome_player2;
    
    std::cout<<"\t\tPlayer 1 insira o seu nome (Lembrando que tem que ser o mesmo do seu mapa)"<<std::endl;
    std::cin>>nome_player1;
    player1.set_nome(nome_player1);
    std::cout<<"\n\n";

    std::cout<<"\t\tPlayer 2 insira o seu nome (Lembrando que tem que ser o mesmo do seu mapa)"<<std::endl;
    std::cin>>nome_player2;
    player2.set_nome(nome_player2);

    system("clear");
}

void Partida::Tamanho_mapa(){
    std::cout<<"Insira o tamanho desejado do mapa. recomenda-se entre 7 e 20\n";
    int tam;
    std::cin>>tam;
    player1.Set_Tamanho_Mapa(tam);
    player2.Set_Tamanho_Mapa(tam);
}

bool Partida::checa_ganhador(Player *player){
    if (player->get_pontos()>=player->Get_Embarcacoes()){
        return true;
    }
    return false;
}

void Partida::Declara_Vencedor(Player *player){
    std::cout<<"Parabens ....... "<<player->get_nome()<<std::endl;
    std::cout<<"Voce destruiu todas as embarcações do seu oponente e é o vencedor"<<std::endl;
    usleep(20000000);
}