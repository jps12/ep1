#include "Canoa.hpp"
#include <iostream>
#include <utility> //pair

using namespace std;

Canoa::Canoa(){
    vida = true;
    //cout<<"canoa criada"<<endl;
}

Canoa::~Canoa(){
    //cout<<"Canoa destruida"<<endl;
}

bool Canoa::is_crash(int linha, int coluna){
    std::pair<int, int> local{linha, coluna};
    return !vida;
}

void Canoa::get_caractere(int linha, int coluna){
    std::pair<int, int> local {linha, coluna};
    std::cout<< " 🚣  █";
}

bool Canoa::recebe_dano(int linha, int coluna){
    std::pair<int, int> local{linha, coluna};
    vida = false;
    std::cout<<"Canoa Destruida"<<std::endl;
    return vida;
}