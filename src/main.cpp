#include <iostream>
#include "Menu.hpp"
#include "Partida.hpp"

int main(){
    while (true){
        Menu *menu;
        menu = new Menu;
        Partida partida;
        int resposta = menu->get_resposta();
        delete menu;
        if (resposta==1){
            partida.preparar_partida();
            partida.Game();
        }
        else if (resposta==2){
            partida.Inserir_Mapa();
            partida.Game();
        }
        else if (resposta==0){
            system("clear");
            std::cout<<"Ate a próxima..........."<<std::endl;
            break;
        }
    }
    return 0;
}
