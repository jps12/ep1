#include "Porta_Avioes.hpp"
#include <iostream>

Porta_Avioes::Porta_Avioes(){
    for (int i=0; i<4; i++){
        vida[i]=true;
    }
}

Porta_Avioes::~Porta_Avioes(){

}

void Porta_Avioes::get_caractere(int linha, int coluna){
    std::pair<int, int> local{linha ,coluna};
    for (int i=0; i<4; i++){
        if (local==posicao[i]){
            std::cout<< " ⛴  █";
        }
    }
}

bool Porta_Avioes::is_crash(int linha, int coluna){
    std::pair<int, int> local{linha, coluna};
    for (int i=0; i<4; i++){
        if (local==posicao[i]){
            return !vida[i];
        }
    }
    return false;
}

void Porta_Avioes::set_orientacao(std::pair<int, int> sentido){
    for (int i=1;i<4;i++){
        posicao.push_back({posicao[0].first + sentido.first*i, posicao[0].second + sentido.second*i});
    }
}

bool Porta_Avioes::recebe_dano(int linha, int coluna){
    std::pair<int, int> local {linha, coluna};
    int qtde_vida=0;
    for (int i=0; i<4; i++){
        if (posicao[i].first == linha&&posicao[i].second==coluna){
            srand((unsigned)time(0));
            int Abate_missel = rand() % 2;
            if (Abate_missel){
                vida[i]=false;
                std::cout<<"Acertou um Porta_Avioes"<<std::endl;

            }
            else {
                std::cout<<"O seu ataque foi repelido pelo Porta_Avioes"<<std::endl;
            }
        }
        if (vida[i]){
            qtde_vida++;
        }
    }
    std::cout<<qtde_vida<<"/4 restante"<<std::endl;
    if (qtde_vida==0){
        std::cout<<"Porta_Avioes abatido!!!!!"<<std::endl;
        return false;
    }
    return true;
}