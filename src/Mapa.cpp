#include "Mapa.hpp"
#include <iostream> // cout
#include <fstream> // leitura de arquivo
#include <string> 
#include <sstream> // separar as strings de uma linha
#include <utility> // pair

Mapa::Mapa(){
tam_mapa = 13;
canoa.reserve(40);
submarino.reserve(40);
porta_avioes.reserve(40);
num_embarcacoes=0;
}
Mapa::~Mapa(){

}

void Mapa::carrega_mapa(std::string arquivo){
    std::ifstream my_mapa;
    my_mapa.open("doc/"+arquivo);
    
    if (my_mapa.is_open()){ 
        
        while (!my_mapa.eof()){
            
            std::string linha;
            std::getline(my_mapa, linha);
            
            if (linha[0]=='#'||linha==""){
                continue;
            }

            std::istringstream ss {linha};
            std::string string_linha, string_coluna, tipo, orientacao;
            int line, coluna;

            ss >> string_linha;
            ss >> string_coluna;

            line = stoi(string_linha);
            coluna = stoi(string_coluna);

            ss >> tipo;
            ss >> orientacao;

            if (tipo=="canoa"){
                set_canoa(line, coluna);
            }
            else if (tipo=="submarino"){
                set_submarino(line, coluna, orientacao);
            }
            else if (tipo=="porta_avioes"){
                set_porta_avioes(line, coluna, orientacao);
            }

        }
        my_mapa.close();
    }
    
    else {
        my_mapa.close();
        std::cout<<"Problemas ao carregar o mapa"<<std::endl;
        std::cout<<"Tente novamente!!!!"<<std::endl;
        
    }
}

void Mapa::set_canoa(int posicao_linha, int posicao_coluna){
    num_embarcacoes++;
    canoa1.set_posicao(posicao_linha, posicao_coluna);
    canoa.push_back(canoa1);
    navios[{posicao_linha, posicao_coluna}] = &canoa.back();
}

void Mapa::set_submarino(int posicao_linha, int posicao_coluna, std::string orientacao){
    num_embarcacoes++;
    Submarino submarino1; // Variavel apenas para alterar a posicao;
    submarino1.set_posicao(posicao_linha, posicao_coluna);
    std::pair<int, int> sentido;
    
    sentido = Retorna_Vetor_Orientacao(orientacao);
    submarino1.set_orientacao(sentido);
    
    submarino.push_back(submarino1);
    for(int i = 0; i < 2; i++)
    {
        navios[{posicao_linha + (i*sentido.first) ,posicao_coluna + (i*sentido.second)}] = &submarino.back();
    }
    
}

void Mapa::set_porta_avioes(int posicao_linha, int posicao_coluna, std::string orientacao){
    num_embarcacoes++;
    Porta_Avioes porta_avioes1;
    porta_avioes1.set_posicao(posicao_linha, posicao_coluna);
    std::pair<int, int> sentido;
    
    sentido = Retorna_Vetor_Orientacao(orientacao);
    porta_avioes1.set_orientacao(sentido);

    porta_avioes.push_back(porta_avioes1);

    for(int i = 0; i < 4; i++)
    {
        navios[{posicao_linha + (i*sentido.first) ,posicao_coluna + (i*sentido.second)}] = &porta_avioes.back();
    }
}

void Mapa::imprime_mapa(){
    int i, j;

    for (i=0; i<=tam_mapa; i++){
        for (j=0; j<=tam_mapa; j++){
            if (i==0){
                printf(" %.2d █",j);
            }
            else if (j==0){
                printf(" %.2d █", i);
            }
            else {
                if (tiro_agua[{i, j}]){
                    std::cout<<" 🚫  █";
                }
                else if (navios[{i, j}]==NULL||!navios[{i, j}]->is_crash(i, j)){
                    std::cout<<" 💧  █";
                }
                
                else if(navios[{i, j}]!=NULL) {
                    navios[{i, j}]->get_caractere(i, j);
                }
                
            }
        }
        std::cout<<"\n";
    }
    std::cout<<"\n";
}

std::pair<int, int> Mapa::Retorna_Vetor_Orientacao(std::string orientacao){
    if (orientacao=="cima"){
        return {1, 0};
    }
    if (orientacao=="baixo"){
        return {-1, 0};
    }
    if (orientacao=="esquerda"){
        return {0, -1};
    }
    if (orientacao=="direita"){
        return {0, 1};
    }
}

void Mapa::TirosNaAgua(int linha, int coluna){
    tiro_agua[{linha, coluna}] = true;
}

void Mapa::Set_Tamanho_Mapa(int tam_mapa){
    this->tam_mapa=tam_mapa;
}

void Mapa::Set_Numero_Embarcacoes(int qtde_canoa, int qtde_submarino, int qtde_porta_avioes){
    num_embarcacoes = qtde_canoa + qtde_porta_avioes + qtde_submarino;
}

int Mapa::Get_Embarcacoes(){
    return num_embarcacoes;
}