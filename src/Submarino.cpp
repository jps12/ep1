#include "Submarino.hpp"
#include <iostream> // cout
#include <vector>

Submarino::Submarino(){
    vida[0] = 2;
    vida[1] = 2;
    //std::cout<<"submarino criado"<<std::endl;
}

Submarino::~Submarino(){
    //std::cout<<"Submarino destruido"<<std::endl;
}

void Submarino::get_caractere(int linha, int coluna){
    std::pair<int,int> local{linha, coluna};
    for (int i=0;i<2;i++){
        if(posicao[i]==local){
            if (vida[i]==1){
                std::cout<<" d  █";
            }
            else if (vida[i]==0){
                std::cout<< " f  █";
            }
        }
    }
}    



bool Submarino::is_crash(int linha, int coluna){
    std::pair<int,int> local{linha, coluna};
    for (int i=0;i<2;i++){
        if(posicao[i]==local){
            if (vida[i]==2){
                return false;
            }
            else{
                return true;
            }
        }
    }
    return false;
}

void Submarino::set_orientacao(std::pair<int, int> sentido){
    posicao.push_back({posicao[0].first + sentido.first, posicao[0].second + sentido.second});
}

bool Submarino::recebe_dano(int linha, int coluna){
    std::pair<int, int> local {linha, coluna};
    for (int i=0; i<2; i++){
        if (local==posicao[i]){
            vida[i]--;
            if (vida[i]==0){
                std::cout<<"Parte do Subamrino destruida"<<std::endl;
            }
            if (vida[i]==1){
                std::cout<<"Parte do Submarino danificada"<<std::endl;
            }
        }
    }
    int soma_vida=0;
    soma_vida+=vida[0]+vida[1];
    std::cout<<soma_vida<<"/4 restantes"<<std::endl;
    if (soma_vida==0){
        std::cout<<"Submarino Abatido"<<std::endl;
        return false;
    }
    return true;
}