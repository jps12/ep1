#include <iostream> 
#include "Menu.hpp"
#include <unistd.h>


using namespace std;

Menu::Menu(){
    system("clear");
    cout<<" ▄▄▄▄▄▄  ▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄     ▄▄▄▄▄▄ ▄▄▄▄▄▄  ▄    ▄  ▄  ▄▄▄▄▄"<<endl;
    cout<<" █░░░░█  █   █    █       █    █     █      █       █    █  █  █░░░█"<<endl;
    cout<<" █▄▄▄▀   █   █    █       █    █     █      █       █    █  █  █░░░█"<<endl;
    cout<<" █░░░▀█  █▀▀▀█    █       █    █     █▀▀▀▀  ▀▀▀▀▀█  █▀▀▀▀█  █  █▀▀▀▀"<<endl;
    cout<<" █░░░░█  █   █    █       █    █     █           █  █    █  █  █"<<endl;
    cout<<" █▄▄▄▄█  █   █    █       █    █▄▄▄▄ █▄▄▄▄  ▄▄▄▄▄█  █    █  █  █"<<endl;
    cout<<endl<<endl;
    cout<<"\t\t                 ▄▄▄▄▄▄              ░░ "<<endl;
    cout<<"\t\t                 ███░░██            ░░░░ "<<endl;
    cout<<"\t\t                 ███░░░██          ▄▄▄▄▄▄ "<<endl;
    cout<<"\t\t                 ███░░░░██         ██████"<<endl;
    cout<<"\t\t                 ███░░░░░██        ██████"<<endl;
    cout<<"\t\t                 ███░░░░░░██       ██████"<<endl;    
    cout<<"\t\t                 ███░░░░░░░██       ████ "<<endl;
    cout<<"\t\t                 ███░░░░░░░░██       ██  "<<endl;
    cout<<"\t\t                 ███▀▀▀▀▀▀▀▀▀▀           "<<endl;
    cout<<"\t\t                 ███                    "<<endl;
    cout<<"\t\t▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄███▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄"<<endl;
    cout<<"\t\t██░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░██"<<endl;
    cout<<"\t\t ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░██ "<<endl;
    cout<<"\t\t  ██▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄██  "<<endl;
    cout<<endl<<endl<<endl;
    cout<<"\t\t\tEscolha a sua opcao:"<<endl<<endl<<endl;
    cout<<"Digite '1' Para comecar a jogar com mapas txt prontos"<<endl;
    cout<<"Digite '2' Para inserir manualmente as posições dos Navios"<<endl;
    cout<<"Digite '0' Para sair do jogo"<<std::endl;
    cin>>resposta;
    while(resposta<0||resposta>2){
        cout<<"\t\t\tOpcao invalida, digite novamente"<<endl;
        cin>>resposta;
    }
    system("clear");
}
Menu::~Menu(){

}
int Menu::get_resposta(){
    return resposta;
}