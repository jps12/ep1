#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "Mapa.hpp"

class Player: public Mapa{
    private:
        
        // É a pontuacao de cada jogador
        int pontos;
        // É o nome do jogador
        std::string nome;
    
    public:
        
        Player();
        ~Player();
    
        // Retorna os pontos que o player tem    
        int get_pontos();
        // Incrementa a quantidade de pontos do player
        void acertou_tiro();
        // checa se tem algo onde o outro jogador atacou e chama a funcao Recebe_dano caso tenha algum navio
        bool recebe_tiro(int linha, int coluna);
        // retorna o nome do arquivo onde o mapa está
        std::string get_mapa();
        // funcao que altera o nome do player
        void set_nome(std::string nome);
        // funcao que retorna o nome do jogador
        std::string get_nome();
        // Esta função vai ler o mapa que o jogador inserir
        void Le_Mapa(int qtde_canoa, int qtde_porta_avioes, int qtde_submarinos);
};

#endif