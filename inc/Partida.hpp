#ifndef PARTIDA_HPP
#define PARTIDA_HPP
#include "Player.hpp"

class Partida {
    private:

        // a partida terá dois players
        Player player1, player2;

        void Vez_Jogador(Player *, Player *);
    
    public: 
    
        Partida();
        ~Partida();
    
        // Essa funcao vai carregar os mapas já existentes
        void preparar_partida();

        // Essa funcao é onde o jogo vai rodar
        void Game();

        // Esta função irá ler da entrada e posicionar os navios
        void Inserir_Mapa();

        // Esta função irá receber os nomes dos jogadores;
        void Configura_Player();

        // Esta função irá alterar o tamanho do mapa
        void Tamanho_mapa();

        // Checa se alguem ganhou;
        bool checa_ganhador(Player *player);

        // Declara o vencedor
        void Declara_Vencedor(Player *player);
};

#endif