#ifndef CANOA_HPP
#define CANOA_HPP

#include "Navio.hpp" // Class mãe

class Canoa: public Navio{
    private:
    
    // Indica se a Canoa esta viva ou não
    bool vida;
    
    public:
    
    // Métodos construtores e destrutores
    Canoa();
    ~Canoa();

    // São funções que são padrões de Navio, mas implementação diferente em cada classe filha
    bool is_crash(int linha, int coluna);
    void get_caractere(int linha, int coluna);
    bool recebe_dano(int linha, int coluna);
};
#endif