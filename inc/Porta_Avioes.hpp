#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include "Navio.hpp" // classe mãe
#include <utility> // pair

class Porta_Avioes: public Navio{
    private: 
    // é a vida em cada casa do porta avioes
    bool vida[4];

    public:
    Porta_Avioes();
    ~Porta_Avioes();

    // Preenche o restante das casas do Porta Avioes
    void set_orientacao(std::pair<int, int> sentido);

    // funcoes padroes de navio
    bool is_crash(int linha, int coluna);
    void get_caractere(int linha, int coluna);
    bool recebe_dano(int linha, int coluna);
};
#endif