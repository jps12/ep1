#ifndef MAPA_HPP
#define MAPA_HPP
#include "Navio.hpp"
#include "Submarino.hpp"
#include "Porta_Avioes.hpp"
#include "Canoa.hpp"
#include <string> 
#include <utility> // pair
#include <map>
#include <vector>
class Mapa{
    protected:
        Canoa canoa1;
        // É onde é armazenado os endereços de memoria dos navios (submarino, canoa e porta_avioes)
        std::map<std::pair<int, int>, Navio*> navios;
        
        // funcao que altera a posicao da canoa
        void set_canoa(int posicao_linha, int posicao_coluna);
        
        // funcao que altera a posicao do submarino
        void set_submarino(int posicao_linha, int posicao_coluna, std::string orientacao);
        
        //Funcao que altera a posicao do porta avioes
        void set_porta_avioes(int posicao_linha, int posicao_coluna, std::string orientacao);

        // Altera a quantidade de embarcações
        void Set_Numero_Embarcacoes(int qtde_canoa, int qtde_submarino, int qtde_porta_avioes);

        // Essa funcao representa os tiros na água
        std::map<std::pair<int, int>, bool> tiro_agua;
        
    private:
        int num_embarcacoes;
        
        
        // declaração dos navios
        std::vector<Canoa> canoa;
        std::vector<Porta_Avioes> porta_avioes;
        std::vector<Submarino> submarino;

        // é o tamanho do mapa
        int tam_mapa;

        // Dada uma string como "cima", essa funcao retorna um vetor unitario indicando o sentido
        std::pair<int, int> Retorna_Vetor_Orientacao(std::string orientacao);
    
    public:
    
        Mapa();
        ~Mapa();
    
        // Essa funcao vai carregar o mapa de cada jogador
        void carrega_mapa(std::string arquivo);
       
        // Essa funcao vai imprimir o mapa de cada jogador na sua vez;
        void imprime_mapa();

        // Memoriza os tiros na agua
        void TirosNaAgua(int linha, int coluna);

        // Altera o tamanho do mapa
        void Set_Tamanho_Mapa(int tam_mapa);

        // Retorna o numero de embarcacoes
        int Get_Embarcacoes();
    
};

#endif