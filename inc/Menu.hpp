#ifndef  MENU_HPP
#define MENU_HPP

class Menu
{
private:
    //Sera a Resposta do usuário 
    int resposta;
public:
    // O construtor vai imprimir na tela o menu com o "desenho"
    Menu();
    ~Menu();
    // metodo que devolve o que o usuário escolheu
    int get_resposta();
};
#endif 