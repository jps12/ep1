#ifndef NAVIO_HPP
#define NAVIO_HPP

#include <vector>// vector
#include <utility> // pair


class Navio {
    protected:
    // As posicoes de cada navio
    std::vector<std::pair<int, int>> posicao;
   
    // orientacao de cada navio
    std::pair<int, int> orientacao;

    public:
    Navio();
    ~Navio();
   
    // funcao que altera a posicao 
    void set_posicao(int posicao_linha, int posicao_coluna);
   
    // metodo usado apenas para testes
    void print_posicao();

    // a funcao retorna se o navio naquela coordenada está destuído
    virtual bool is_crash(int linha, int coluna)=0;
   
    // retorna um caractere específico de cada navio
    virtual void get_caractere(int linha, int coluna)=0;
   
    // Essa funcao muda a vida de cada navio na posicao recebida
    virtual bool recebe_dano(int linha, int coluna)=0;
};

#endif