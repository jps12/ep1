#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "Navio.hpp"
#include <utility>

class Submarino: public Navio{
    private:
    
    //a vida de cada parte do submarino 
    int vida[2];
    
    public:
    Submarino();
    ~Submarino();

    void set_orientacao(std::pair<int, int> sentido);

    bool is_crash(int linha, int coluna);
    void get_caractere(int linha, int coluna);
    bool recebe_dano(int linha, int coluna);
};

#endif