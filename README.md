# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```


# Descrição do projeto:

O jogo começa com o menu do jogo com o usuário podendo escolher entre:
    * Carregar mapas preexistentes em arquivos txt armazenados na pasta doc/. Cada jogador terá seu próprio arquivo contendo as posições das suas embarcações. E começar o jogo logo em seguida.
    * Digitar as posições das embarcações no próprio programa em execução e jogar em seguida.
    * Por fim ele poderar encerrar a execução do programa.

## Regras:

* Os turnos são alternados independente do acerto ou não de outra embarcação.
* O jogador que começa a partida é definido aleatoriamente.
* As embarcações devem estar dentro do tamanho do mapa escolhido pelo usuário no início do jogo.
* Em cada turno o jogador deverá inserir a linha e coluna do seu ataque
* Hverá um vencendor quando um jogador for o primeiro a destruir todas as embarcações do seu oponente.

## Estrutura do mapa:

O mapa tem que ter a seguinte estrutura:
* "Linha" "Coluna" "Navio" "Orientação" 
No caso da canoa não é necessário digitar nada em "Orientação". Poderá tbm ser feitos comentários desde que no início da linha tenha o caractere '#'. O nome do arquivo tem que ser o nome do player + ".txt", por exemplo, um jogador "joao", tem que criar o seu mapa como "joao.txt" para o programa poder ler o corretamente o mapa.